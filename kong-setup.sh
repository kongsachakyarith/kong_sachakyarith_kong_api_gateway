#create services
curl -i -s -X POST http://localhost:8001/services \
  --data name=user-service \
  --data url='http://110.74.194.123:6070/webjars/swagger-ui/index.html'

curl -i -s -X POST http://localhost:8001/services \
  --data name=task-service \
  --data url='http://110.74.194.123:6072/webjars/swagger-ui/index.html'

#create routes
curl - -X POST \
	—-url http://localhost:8001/services/task-service/routes \
	—-data 'name=task-api' \
	--data 'paths[]=/' \
	--data 'strip_path=false'

curl -i -X POST \
    --url http://localhost:8001/services/user-service/routes \
    --data 'name=user-api' \
    --data 'paths[]=/' \
    --data 'strip_path=false'

curl -i -X POST \
    --url http://localhost:8001/services/user-service/routes \
    --data 'name=group-api' \
    --data 'paths[]=/' \
    --data 'strip_path=false'


HOST="https://auth.saintrivers.tech"
REALM="control-tower"

TOKEN=$(curl -s --location --request POST "$HOST/auth/realms/$REALM/protocol/openid-connect/token" \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data-urlencode 'grant_type=client_credentials' \
  --data-urlencode 'client_id=control-tower-microservice' \
  --data-urlencode 'client_secret=CQpcpyUu3fdYqAazbuEqtmrkrQqibkBC' \
  --data-urlencode 'scopes=code' | jq -r '.access_token')

  curl  -s -H "Authorization: Bearer ${TOKEN}" "http://localhost:8000/api/v1/tasks" | jq


 #3 rate limiting
curl -X POST http://localhost:8001/plugins/ \
  --data "name=rate-limiting"  \
    --data "config.minute=5" \
    --data "config.hour=10000" \
    --data "config.policy=local"